LICENSE="MIT"
FILESPATHBASE_LAYER := "${@os.path.dirname(bb.data.getVar('FILE', d, True))}"
LIC_FILES_CHKSUM = "file://${FILESPATHBASE_LAYER}/../../LICENSE;md5=3ef3bc6a418b731c5c8b05d31db110eb"

IMAGE_FEATURES="package-management"

DISTRO_SSH_DAEMON ?= "dropbear"
DISTRO_PACKAGE_MANAGER ?= "opkg opkg-collateral"

IMAGE_LINGUAS = "en-gb de-de fr-fr"

IMAGE_INSTALL += " \
	task-core-boot \
	\
	${DISTRO_PACKAGE_MANAGER} \
	${DISTRO_SSH_DAEMON} \
	openssh-sftp-server \
	avahi-daemon \
	avahi-autoipd \
	mtd-utils \
	gdbserver \
	strace \
	qt4-examples \
	xserver-xorg \
	xinit \
	xf86-input-evdev \
	xf86-video-fbdev \
	xinput-calibrator \
	liberation-fonts \
	lighttpd \
	lighttpd-module-alias \
	lighttpd-module-auth \
	lighttpd-module-cgi \
	lighttpd-module-compress \
	lighttpd-module-fastcgi \
	lighttpd-module-evhost \
	lighttpd-module-redirect \
	lighttpd-module-rewrite \
	lighttpd-module-setenv \
	lighttpd-module-simple-vhost \
	fastcgi \
	kernel-modules \
	module-init-tools \
	dtc \
	${@base_contains('MACHINE_FEATURES', 'ext2', 'task-base-ext2', '', d)} \
	${@base_contains('MACHINE_FEATURES', 'usbhost', 'task-base-usbhost', '', d)} \
	${@base_contains('MACHINE_FEATURES', 'usbgadget', 'task-base-usbgadget', '', d)} \
	${@base_contains('MACHINE_FEATURES', 'vfat', 'task-base-vfat dosfstools', '', d)} \
	${@base_contains('DISTRO_FEATURES', 'ppp', 'task-base-ppp', '', d)} \
"

IMAGE_INSTALL_append_stamp9g45 = " \
	boots-utils \
"

export IMAGE_BASENAME="taskit-demo-x11-image"

inherit image

# QT Toolchain

QTNAME = "qt"
QT_DIR_NAME = "qt4"
TOOLCHAIN_HOST_TASK += "nativesdk-packagegroup-${QTNAME}-toolchain-host"
QT_TOOLS_PREFIX = "${SDKPATHNATIVE}${bindir_nativesdk}"

toolchain_create_sdk_env_script_append() {
    echo 'export OE_QMAKE_CFLAGS="$CFLAGS"' >> $script
    echo 'export OE_QMAKE_CXXFLAGS="$CXXFLAGS"' >> $script
    echo 'export OE_QMAKE_LDFLAGS="$LDFLAGS"' >> $script
    echo 'export OE_QMAKE_CC=$CC' >> $script
    echo 'export OE_QMAKE_CXX=$CXX' >> $script
    echo 'export OE_QMAKE_LINK=$CXX' >> $script
    echo 'export OE_QMAKE_AR=$AR' >> $script
    echo 'export OE_QMAKE_LIBDIR_QT=${SDKTARGETSYSROOT}/${libdir}' >> $script
    echo 'export OE_QMAKE_INCDIR_QT=${SDKTARGETSYSROOT}/${includedir}/${QT_DIR_NAME}' >> $script
    echo 'export OE_QMAKE_MOC=${QT_TOOLS_PREFIX}/moc4' >> $script
    echo 'export OE_QMAKE_UIC=${QT_TOOLS_PREFIX}/uic4' >> $script
    echo 'export OE_QMAKE_UIC3=${QT_TOOLS_PREFIX}/uic34' >> $script
    echo 'export OE_QMAKE_RCC=${QT_TOOLS_PREFIX}/rcc4' >> $script
    echo 'export OE_QMAKE_QDBUSCPP2XML=${QT_TOOLS_PREFIX}/qdbuscpp2xml4' >> $script
    echo 'export OE_QMAKE_QDBUSXML2CPP=${QT_TOOLS_PREFIX}/qdbusxml2cpp4' >> $script
    echo 'export OE_QMAKE_QT_CONFIG=${SDKTARGETSYSROOT}/${datadir}/${QT_DIR_NAME}/mkspecs/qconfig.pri' >> $script
    echo 'export QMAKESPEC=${SDKTARGETSYSROOT}/${datadir}/${QT_DIR_NAME}/mkspecs/linux-g++' >> $script
    echo 'export QT_CONF_PATH=${SDKPATHNATIVE}/${sysconfdir}/qt.conf' >> $script

    # make a symbolic link to mkspecs for compatibility with Nokia's SDK
    # and QTCreator
    (cd ${SDK_OUTPUT}/${QT_TOOLS_PREFIX}/..; ln -s ${SDKTARGETSYSROOT}/usr/share/${QT_DIR_NAME}/mkspecs mkspecs;)
}

