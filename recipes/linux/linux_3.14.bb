require linux.inc

PR="r0"

COMPATIBLE_MACHINE = "stampa5d36|stamp9x5"
DEFAULT_PREFERENCE = "1"

SRC_URI = " \
	${KERNELORG_MIRROR}/linux/kernel/v3.0/linux-3.14.tar.xz;name=kernel \
	file://mmc-fixed-naming.patch \
	file://defconfig \
"
SRC_URI_append_stampa5d36 = " \
	file://workaround-standby-bug.patch \
"

SRC_URI[kernel.md5sum] = "b621207b3f6ecbb67db18b13258f8ea8"
SRC_URI[kernel.sha256sum] = "61558aa490855f42b6340d1a1596be47454909629327c49a5e4e10268065dffa"
