require linux.inc

PR="r0"

COMPATIBLE_MACHINE = "stampa5d36"
DEFAULT_PREFERENCE = "1"

SRC_URI = " \
	${KERNELORG_MIRROR}/linux/kernel/v3.0/linux-3.16.tar.xz;name=kernel \
	file://0001-mfd-add-atmel-hlcdc-driver.patch \
	file://0002-mfd-add-documentation-for-atmel-hlcdc-DT-bindings.patch \
	file://0003-pwm-add-support-for-atmel-hlcdc-pwm-device.patch \
	file://0004-pwm-add-DT-bindings-documentation-for-atmel-hlcdc-pw.patch \
	file://0005-drm-add-Atmel-HLCDC-Display-Controller-support.patch \
	file://0006-drm-add-DT-bindings-documentation-for-atmel-hlcdc-dc.patch \
	file://0007-ARM-AT91-dt-split-sama5d3-lcd-pin-definitions-to-mat.patch \
	file://0008-ARM-AT91-dt-add-alternative-pin-muxing-for-sama5d3-l.patch \
	file://0009-ARM-at91-dt-define-the-HLCDC-node-available-on-sama5.patch \
	file://0010-ARM-at91-dt-add-LCD-panel-description-to-sama5d3xdm..patch \
	file://0011-ARM-at91-dt-enable-the-LCD-panel-on-sama5d3xek-board.patch \
	file://0012-mmc-Make-device-names-persistent.patch \
	file://0013-drm-atmel_hlcdc-Fix-display-flickering-for-PanelA5-d.patch \
	file://0014-at91-sama5d3x-fix-LCD-muxing.patch \
	file://0015-drm-panel-simple-Change-ETM0700G0DH6-timings-to-make.patch \
	file://0016-pwm-backlight-do-not-use-legacy-API-to-request-PWM.patch \
	file://0017-ARM-at91-dt-add-StampA5D36-include-file.patch \
	file://0018-ARM-at91-dt-add-PanelA5-device-tree-files.patch \
	file://0019-ARM-at91-pm-add-the-socs-support-the-DDRC-controller.patch \
	file://0020-ARM-at91-pm_slowclock-add-enter-standby-mode-for-ARM.patch \
	file://0021-ARM-at91-pm_slowclock-MOR-register-KEY-was-missing.patch \
	file://0022-ARM-at91-pm_slowclock-implement-workaround-for-LPDDR.patch \
	file://0023-ARM-at91-pm_slowclock-Disable-UTMI-PLL-during-suspen.patch \
	file://0024-ARM-at91-dt-add-PortuxA5-device-tree-files.patch \
	file://0025-drm-panel-simple-add-support-for-ETM0350G0BDH6.patch \
	file://0026-ARM-at91-dt-add-support-for-PortuxA5-with-ETM0350G0B.patch \
	file://0027-ARM-at91-dt-fix-USB-device-VBUS-pin-for-PanelA5-and-.patch \
	file://0028-iio-adc-at91-set-Touchscreen-Switches-Closure-Time-t.patch \
	file://0029-ARM-at91-dt-add-UART-definitions-for-PanelA5.patch \
	file://0030-ARM-at91-dt-add-device-tree-for-resistive-BoTouch-Pa.patch \
	file://0031-can-at91_can-add-missing-prepare-and-unprepare-of-th.patch \
	file://0032-mtd-atmel_nand-NFC-support-multiple-interrupt-handli.patch \
	file://0033-ARM-at91-dt-Fix-typo-regarding-can0_clk.patch \
	file://0034-pwm-atmel-Fix-calculation-of-prescale-value.patch \
	file://defconfig \
"

SRC_URI[kernel.md5sum] = "5c569ed649a0c9711879f333e90c5386"
SRC_URI[kernel.sha256sum] = "4813ad7927a7d92e5339a873ab16201b242b2748934f12cb5df9ba2cfe1d77a0"

KERNEL_DEVICETREE_stampa5d36 = " \
	arch/${ARCH}/boot/dts/panela5-capacitive.dts \
	arch/${ARCH}/boot/dts/panela5-resistive.dts \
	arch/${ARCH}/boot/dts/panela5-resistive-botouch.dts \
	arch/${ARCH}/boot/dts/portuxa5.dts \
	arch/${ARCH}/boot/dts/portuxa5-etm035.dts \
"
